// Babel Require Hookを通してpower-assertを使ったES2015コードを変換する
require('babel-register')({
  only: /spec/,
  plugins: ['babel-plugin-espower'],
  // SyntaxError がでるため
  // SyntaxError: Unexpected token import
  presets: ['env'],
  extensions: ['.es6', '.js']
});
// Error がでるため
// E/launcher - Error: ReferenceError: regeneratorRuntime is not defined
require('babel-polyfill');

exports.config = {
  // specの実行前に、protractorが準備できたら一度だけ呼ばれる関数
  onPrepare: function () {
    browser.ignoreSynchronization = true;
  },

  directConnect: true,

  capabilities: {
    'browserName': 'chrome',
    'chromeOptions': {
      args: [
        // '--headless',
        // '--disable-gpu',
        '--window-size=800,600'
      ]
    }
  },

  framework: 'mocha',

  suites: {
    test1: './spec/test1/*_spec.js',
    test2: './spec/test2/*_spec.js'
  },

  mochaOpts: {
    ui: 'bdd',
    reporter: 'spec',
    slow: 3000,
    enableTimeouts: false
  }
};
