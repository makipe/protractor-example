## 参考・出典
### 概要
- [Seleniumアレルギーのための処方箋 - Qiita](https://qiita.com/cognitom/items/27b7375bea653b414c8f)

### node
- [Command Line Options | Node.js v9.3.0 Documentation](https://nodejs.org/api/cli.html#cli_r_require_module)

### Protractor
- [Not Angularな環境でProtractorを使ってES2015(ES6), ES7なE2Eテストコードを書いてみた - Qiita](https://qiita.com/nana4gonta/items/fabf1ab30ef16050c1e8)
- [protractor/config.ts at master · angular/protractor](https://github.com/angular/protractor/blob/master/lib/config.ts)
- [Protractor - end-to-end testing for AngularJS](http://www.protractortest.org/#/api)
- [Protractor: AngularJSの次世代E2Eテストフレームワーク - Qiita](https://qiita.com/zoetro/items/b72130960c39055b8474)

### JavaScript
- [JavaScriptのasync/awaitがPromiseよりもっと良い - Qiita](https://qiita.com/Anders/items/dfcb48d8b27ceaffb443)
